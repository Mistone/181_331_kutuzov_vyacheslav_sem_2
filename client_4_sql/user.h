#ifndef USER_H
#define USER_H

#include <QTcpSocket>
#include <QCloseEvent>
#include <QWidget>
#include <QTableWidget>

class QTextEdit;
class QLineEdit;

class user: public QWidget {
    Q_OBJECT
private:

    quint16 _nextBlockSize;
    QTableWidget _table;
    QTextEdit *_textInfo;
    QLineEdit *_textInput;

public:
    user(const QString &strHost, quint16 port);
    QByteArray sha256(const QByteArray& text);
    QWidget *_cur_win;
    QTcpSocket *_tcpSocket;
    QString _login;
public slots:
    void slotReadyRead();
    void slotSendToServer(QString to_send);
    void slotConnected();
signals:
    void table_update(QTableWidget *table);
    void success_auth();
};


#endif // USER_H
