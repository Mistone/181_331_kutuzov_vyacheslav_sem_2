/********************************************************************************
** Form generated from reading UI file 'admin_main_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMIN_MAIN_WINDOW_H
#define UI_ADMIN_MAIN_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Admin_Main_Window
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *Worker_list;
    QPushButton *User_list;
    QPushButton *Medicine_list;
    QPushButton *pushButton;

    void setupUi(QDialog *Admin_Main_Window)
    {
        if (Admin_Main_Window->objectName().isEmpty())
            Admin_Main_Window->setObjectName(QString::fromUtf8("Admin_Main_Window"));
        Admin_Main_Window->resize(815, 479);
        verticalLayoutWidget = new QWidget(Admin_Main_Window);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(160, 80, 491, 311));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        Worker_list = new QPushButton(verticalLayoutWidget);
        Worker_list->setObjectName(QString::fromUtf8("Worker_list"));

        verticalLayout->addWidget(Worker_list);

        User_list = new QPushButton(verticalLayoutWidget);
        User_list->setObjectName(QString::fromUtf8("User_list"));

        verticalLayout->addWidget(User_list);

        Medicine_list = new QPushButton(verticalLayoutWidget);
        Medicine_list->setObjectName(QString::fromUtf8("Medicine_list"));

        verticalLayout->addWidget(Medicine_list);

        pushButton = new QPushButton(verticalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);


        retranslateUi(Admin_Main_Window);

        QMetaObject::connectSlotsByName(Admin_Main_Window);
    } // setupUi

    void retranslateUi(QDialog *Admin_Main_Window)
    {
        Admin_Main_Window->setWindowTitle(QApplication::translate("Admin_Main_Window", "Dialog", nullptr));
        Worker_list->setText(QApplication::translate("Admin_Main_Window", "\320\241\320\277\320\270\321\201\320\276\320\272 \321\200\320\260\320\261\320\276\321\202\320\275\320\270\320\272\320\276\320\262", nullptr));
        User_list->setText(QApplication::translate("Admin_Main_Window", "\320\241\320\277\320\270\321\201\320\276\320\272 \320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\320\265\320\271", nullptr));
        Medicine_list->setText(QApplication::translate("Admin_Main_Window", "\320\241\320\277\320\270\321\201\320\276\320\272 \320\274\320\265\320\264\320\270\320\272\320\260\320\274\320\265\320\275\321\202\320\276\320\262", nullptr));
        pushButton->setText(QApplication::translate("Admin_Main_Window", "\320\230\321\201\321\202\320\276\321\200\320\270\321\217 \320\277\320\276\320\272\321\203\320\277\320\276\320\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Admin_Main_Window: public Ui_Admin_Main_Window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMIN_MAIN_WINDOW_H
