#include "data.h"
#include "user_main_window.h"
#include "worker_main_window.h"
#include "admin_main_window.h"
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QMessageBox>

Data::Data()
{

}

Data::~Data()
{

}

void Data::download_all(){
    Admins = download_admins();
    Clients = download_users();
    Workers = download_workers();
    Drug = download_drug();
    History_cart = download_history_cart();
}


Container<Admin> Data::download_admins(){
    Container<Admin> Admins;
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\Admin_auth.txt");
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return Admins;
        QTextStream in(&file);
        QString line = in.readLine();
        while (!line.isNull()) {
            QStringList memory = line.split(";");
            Admin admin(memory.at(0),
                        memory.at(1),
                        memory.at(2),
                        memory.at(3));
            Admins.add(admin);
            line = in.readLine();
        }
        return Admins;
}

Container<Client> Data::download_users(){
    Container<Client> Clients;
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\Users_auth.txt");
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return Clients;
        QTextStream in(&file);
        QString line = in.readLine();
        while (!line.isNull()) {
            QStringList memory = line.split(";");
            Client client(memory.at(0),
                        memory.at(1),
                        memory.at(2),
                        memory.at(3));
            Clients.add(client);
            line = in.readLine();
        }
        return Clients;
}

Container<Worker> Data::download_workers(){
    Container<Worker> Workers;
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\Worker_auth.txt");
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return Workers;
        QTextStream in(&file);
        QString line = in.readLine();
        while (!line.isNull()) {
            QStringList memory = line.split(";");
            Worker worker(memory.at(0),
                        memory.at(1),
                        memory.at(2),
                        memory.at(3));
            Workers.add(worker);
            line = in.readLine();
        }
        return Workers;
}

Container<Medicines> Data::download_drug(){
    Container<Medicines> Drug;
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\drug.txt");
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return Drug;
        QTextStream in(&file);
        QString line = in.readLine();
        while (!line.isNull()) {
            QStringList memory = line.split(";");
            Medicines drug(//memory.at(0).toInt(),
                        memory.at(0),
                        memory.at(1),
                        memory.at(2),
                        memory.at(3).toInt());
            Drug.add(drug);
            line = in.readLine();
        }
        return Drug;
}

Container<history_cart> Data::download_history_cart(){
    Container<history_cart> History_cart;
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\history_cart.txt");
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return History_cart;
        QTextStream in(&file);
        QString line = in.readLine();
        while (!line.isNull()) {
            QStringList memory = line.split(";");
            history_cart cart(memory.at(0),
                        memory.at(1),
                        memory.at(2));
            History_cart.add(cart);
            line = in.readLine();
        }
        return History_cart;
}

void Data::upload_all(){
    upload_admins();
    upload_drug();
    upload_users();
    upload_workers();
    upload_history_cart();
}

void Data::upload_admins(){
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\Admin_auth.txt");
       if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
           return;

       QTextStream out(&file);
       for(unsigned long long i = 0; i < Admins.size(); i++){
           out << Admins.cont.at(i).Login + ";" + Admins.cont.at(i).Password + ";"
                  + Admins.cont.at(i).Fio + ";" + Admins.cont.at(i).Phone_number + "\n";
       }
}

void Data::upload_drug(){
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\drug.txt");
       if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
           return;

       QTextStream out(&file);
       for(unsigned long long i = 0; i < Drug.size(); i++){
           out << Drug.cont.at(i).Name + ";" + Drug.cont.at(i).Kind + ";"
                  + Drug.cont.at(i).Price + ";" + QString::number(Drug.cont.at(i).Amount) + "\n";
       }
}

void Data::upload_users(){
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\Users_auth.txt");
       if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
           return;

       QTextStream out(&file);
       for(unsigned long long i = 0; i < Clients.size(); i++){
           out << Clients.cont.at(i).Login + ";" + Clients.cont.at(i).Password + ";"
                  + Clients.cont.at(i).Fio + ";" + Clients.cont.at(i).Phone_number + "\n";
       }
}

void Data::upload_workers(){
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\Worker_auth.txt");
       if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
           return;

       QTextStream out(&file);
       for(unsigned long long i = 0; i < Workers.size(); i++){
           out << Workers.cont.at(i).Login + ";" + Workers.cont.at(i).Password + ";"
                  + Workers.cont.at(i).Fio + ";" + Workers.cont.at(i).Phone_number + "\n";
       }
}

void Data::upload_history_cart(){
    QFile file("C:\\Users\\User\\Desktop\\Medicine\\history_cart.txt");
       if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
           return;

       QTextStream out(&file);
       for(unsigned long long i = 0; i < History_cart.size(); i++){
           out << History_cart.cont.at(i).FIO + ";" + History_cart.cont.at(i).Medicine + ";"
                  + History_cart.cont.at(i).Count + "\n";
       }
}

template <class T>
void Data::reg(T user, QString lvl_of_access){ // ToDo переделать метод для вариативности регистрации (реализация админского метода)
    error = nullptr;
    if(user.Password.size() < 8){
        error = "size of password";
    }

    else if(user.Phone_number.size() != 12){
        error = "phone number size";
    }

    else if(user.Phone_number.data()[0] != "+" || user.Phone_number.data()[1] != "7"){
        error = "format of number";
    }

    else{
        if(lvl_of_access == "user"){
            QFile file("C:\\Users\\User\\Desktop\\Medicine\\Users_auth.txt");
            if (file.open(QIODevice::Append | QIODevice::Text)){
                QTextStream out(&file);
                out << '\n' << user.Login << ';' << user.Password
                   << ';' << user.Fio << ';' << user.Phone_number;
                }
            }

        else if(lvl_of_access == "admin"){
            QFile file("C:\\Users\\User\\Desktop\\Medicine\\Admin_auth.txt");
            if (file.open(QIODevice::Append | QIODevice::Text)){
                QTextStream out(&file);
                out << '\n' << user.Login << ';' << user.Password
                   << ';' << user.Fio << ';' << user.Phone_number;
                }
            }

        else if(lvl_of_access == "worker"){
            QFile file("C:\\Users\\User\\Desktop\\Medicine\\Worker_auth.txt");
            if (file.open(QIODevice::Append | QIODevice::Text)){
                QTextStream out(&file);
                out << '\n' << user.Login << ';' << user.Password
                   << ';' << user.Fio << ';' << user.Phone_number;
                }
            }
    }
}

void Data::login(People auth, QString type){ //ToDo сравнивать по if radiobutton и в нужном векторе сравнивать информацию, будет возможно после реализации хранения векторов в нужном месте
    if (type == "User"){
        for(unsigned long long i = 0; i < Clients.cont.size(); i++){
            if(Clients.cont.at(i).Login == auth.Login && Clients.cont.at(i).Password == auth.Password){
                level_of_access = "user";
                User_Main_Window UserW(this, &Clients.cont.at(i));
                UserW.setModal(true);
                UserW.exec();
            }
        }
    }

    else if (type == "Admin") {
        for(unsigned long long i = 0; i < Admins.cont.size(); i++){
            if(Admins.cont.at(i).Login == auth.Login && Admins.cont.at(i).Password == auth.Password){
                level_of_access = "admin";
                Admin_Main_Window AdminW(this, &Admins.cont.at(i));
                AdminW.setModal(true);
                AdminW.exec();
            }
        }
    }

    else if (type == "Worker") {
        for(unsigned long long i = 0; i < Workers.cont.size(); i++){
            if(Workers.cont.at(i).Login == auth.Login && Workers.cont.at(i).Password == auth.Password){
                level_of_access = "worker";
                worker_main_window WorkerW(this, &Workers.cont.at(i));
                WorkerW.setModal(true);
                WorkerW.exec();
            }
        }
    }
}
