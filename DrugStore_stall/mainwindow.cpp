#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_Reg_clicked()
{
   reg_window = new Reg_window(this);
   reg_window->show();
}

void MainWindow::on_Auth_clicked()
{
    this->hide();
    log_window = new login_window(this);
    log_window->show();
}
