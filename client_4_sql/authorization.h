#ifndef AUTHORIZATION_H
#define AUTHORIZATION_H

#include <QWidget>
#include <user.h>
#include <QVBoxLayout>
#include <QComboBox>

namespace Ui {
class authorization;
}

class authorization : public QWidget
{
    Q_OBJECT

public:
    explicit authorization(user *user = nullptr);
    ~authorization();

private slots:
    void on_auth_clicked();
    void change_ui();
    void on_sign_clicked();
    void on_send_clicked();
    void on_delete_clicked();
    void on_find_clicked();
    void table_update(QTableWidget *table);
private:
    Ui::authorization *ui;
    user *_user;
    QLineEdit *line;
    QLineEdit *line1;
    QLineEdit *line2;
    QLineEdit *line_1;
    QComboBox *choice;
    QTableWidget *_table;
    QVBoxLayout *layout_1;
};


#endif // AUTHORIZATION_H
