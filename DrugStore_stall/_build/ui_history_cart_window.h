/********************************************************************************
** Form generated from reading UI file 'history_cart_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HISTORY_CART_WINDOW_H
#define UI_HISTORY_CART_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_history_cart_window
{
public:
    QTableWidget *tableWidget;

    void setupUi(QDialog *history_cart_window)
    {
        if (history_cart_window->objectName().isEmpty())
            history_cart_window->setObjectName(QString::fromUtf8("history_cart_window"));
        history_cart_window->resize(716, 453);
        tableWidget = new QTableWidget(history_cart_window);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 0, 711, 451));

        retranslateUi(history_cart_window);

        QMetaObject::connectSlotsByName(history_cart_window);
    } // setupUi

    void retranslateUi(QDialog *history_cart_window)
    {
        history_cart_window->setWindowTitle(QApplication::translate("history_cart_window", "Dialog", nullptr));
    } // retranslateUi

};

namespace Ui {
    class history_cart_window: public Ui_history_cart_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HISTORY_CART_WINDOW_H
