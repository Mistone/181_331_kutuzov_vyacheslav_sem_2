#ifndef USER_LIST_WINDOW_H
#define USER_LIST_WINDOW_H

#include <QDialog>
#include <QObject>
#include <QDebug>
#include "data.h"

namespace Ui {
class user_list_window;
}

class user_list_window : public QDialog
{
    Q_OBJECT

public:
    explicit user_list_window(Data *_DB = nullptr);
    ~user_list_window();

private slots:
    void on_tableWidget_cellChanged(int row, int column);

private:
    Ui::user_list_window *ui;
    Data *DB;
};

#endif // USER_LIST_WINDOW_H
