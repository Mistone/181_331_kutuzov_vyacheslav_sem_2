#include "reg_window.h"
#include "ui_reg_window.h"
#include "mainwindow.h"
#include "client.h"

Reg_window::Reg_window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Reg_window)
{
    ui->setupUi(this);
}

Reg_window::~Reg_window()
{
    delete ui;
}

void Reg_window::on_Registration_clicked()
{
    QString login = ui->Login->text();
    QString password = ui->Password->text();
    QString fio = ui->FIO->text();
    QString phone = ui->Phone_number->text();

    Client _User(login, password, fio, phone);
    QString lvl = "user";
    Data *DB;
    DB->reg(_User, lvl);

    if(DB->error == "size of password"){
        QMessageBox::information(this, tr("Ошибка регистрации"), tr("Ваш пароль должен быть больше 8 символов"));
    }

    else if(DB->error == "phone number size"){
        QMessageBox::information(this, tr("Ошибка регистрации"), tr("Номер телефона должен содержать 12 символов"));
    }

    else if(DB->error == "format of number"){
        QMessageBox::information(this, tr("Ошибка регистрации"), tr("Введите ваш номер телефона в формате '+71234567890'"));
    }

    else {
        QMessageBox::information(this, tr("Успешная регистрация!"), tr("Поздравляем, вы зарегистрированы"));
        DB->download_all();
        this->hide();
    }
}
