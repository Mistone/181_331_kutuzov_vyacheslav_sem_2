#ifndef LOGIN_WINDOW_H
#define LOGIN_WINDOW_H

#include <QDialog>
#include <people.h>
#include <data.h>
#include <user_main_window.h>

namespace Ui {
class login_window;
}

class login_window : public QDialog
{
    Q_OBJECT

public:
    explicit login_window(QWidget *parent = nullptr);
    ~login_window();

private slots:
    void on_Join_clicked();

private:
    Ui::login_window *ui;
};

#endif // LOGIN_WINDOW_H
