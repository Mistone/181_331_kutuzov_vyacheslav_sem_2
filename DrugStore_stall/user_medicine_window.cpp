#include "QStandardItemModel"
#include "QStandardItem"

#include "user_medicine_window.h"
#include "ui_user_medicine_window.h"
#include "data.h"
#include "medicines.h"
#include "container.h"
#include <QDebug>

User_Medicine_Window::User_Medicine_Window(Data *_DB) :
    ui(new Ui::User_Medicine_Window),
    DB(_DB)
{
    ui->setupUi(this);
    ui->comboBox->addItem("Наименование");
    ui->comboBox->addItem("Тип");
    ui->comboBox->addItem("Цена");
    ui->comboBox->addItem("Количество");



    ui->tableWidget->setRowCount(DB->Drug.size());
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Наименование"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Тип"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Цена"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Количество"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i < DB->Drug.size(); i ++){
        item = new QTableWidgetItem(DB->Drug.cont.at(i).Name);
        ui->tableWidget->setItem(i, 0, item);

        item = new QTableWidgetItem(DB->Drug.cont.at(i).Kind);
        ui->tableWidget->setItem(i, 1, item);

        item = new QTableWidgetItem(DB->Drug.cont.at(i).Price);
        ui->tableWidget->setItem(i, 2, item);

        item = new QTableWidgetItem(QString::number(DB->Drug.cont.at(i).Amount));
        ui->tableWidget->setItem(i, 3, item);
    }
    //ui->tableWidget->resizeRowsToContents();
    //ui->tableWidget->resizeColumnsToContents();
    //ui->tableWidget->blockSignals(oldState);
}










    /*QStandardItemModel *model = new QStandardItemModel;
    QStandardItem *item;

    //Заголовки столбцов
    QStringList horizontalHeader;
    horizontalHeader.append("Наименование");
    horizontalHeader.append("Тип");
    horizontalHeader.append("Цена");
    horizontalHeader.append("Количество");

    //Заголовки строк
    //QStringList verticalHeader;
    //verticalHeader.append("Ряд 1");
    //verticalHeader.append("Ряд 2");

    model->setHorizontalHeaderLabels(horizontalHeader);
    //model->setVerticalHeaderLabels(verticalHeader);


    for(unsigned long long i = 0; i < DB->Drug.size(); i++){
        //for (unsigned int j = 0; j < 4; j ++){
        qDebug() << i;

            item = new QStandardItem(QString(DB->Drug.cont.at(i).Name));
            model->setItem(int(i), 0, item);

            item = new QStandardItem(QString(DB->Drug.cont.at(i).Kind));
            model->setItem(int(i), 1, item);

            item = new QStandardItem(QString(DB->Drug.cont.at(i).Price));
            model->setItem(int(i), 2, item);

            item = new QStandardItem(QString(DB->Drug.cont.at(i).Amount));
            model->setItem(int(i), 3, item);
        //}
    }*/


User_Medicine_Window::~User_Medicine_Window()
{
    delete ui;
}

void User_Medicine_Window::on_tableWidget_cellChanged(int row, int column)
{
    if(DB->level_of_access == "admin"){
        if(column == 0)
            DB->Drug.cont.at(row).Name = ui->tableWidget->item(row, column)->text();
        else if(column == 1)
            DB->Drug.cont.at(row).Kind = ui->tableWidget->item(row, column)->text();
        else if (column == 2)
            DB->Drug.cont.at(row).Price = ui->tableWidget->item(row, column)->text();
        else if(column == 3)
            DB->Drug.cont.at(row).Amount = ui->tableWidget->item(row, column)->text().toInt();
        DB->upload_drug();
    }
}

void User_Medicine_Window::on_pushButton_clicked()
{
    QString kind_of = ui->comboBox->currentText();
    QString text = ui->lineEdit->text();

    Container<Medicines> container;

    if(kind_of == "Наименование"){
        for(unsigned int i = 0; i < DB->Drug.cont.size(); i++){
            if(DB->Drug.cont.at(i).Name == text){
                container.add(DB->Drug.cont.at(i));
            }
        }
    }

    else if(kind_of == "Тип"){
        for(unsigned int i = 0; i < DB->Drug.cont.size(); i++){
            if(DB->Drug.cont.at(i).Kind == text){
                container.add(DB->Drug.cont.at(i));
            }
        }
    }

    else if(kind_of == "Цена"){
        for(unsigned int i = 0; i < DB->Drug.cont.size(); i++){
            if(DB->Drug.cont.at(i).Price == text){
                container.add(DB->Drug.cont.at(i));
            }
        }
    }

    else if(kind_of == "Количество"){
        for(unsigned int i = 0; i < DB->Drug.cont.size(); i++){
            if(DB->Drug.cont.at(i).Amount == text){
                container.add(DB->Drug.cont.at(i));
            }
        }
    }

    if(text == nullptr)
        container = DB->Drug;

    ui->tableWidget->setRowCount(container.size());
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Наименование"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Тип"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Цена"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Количество"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i < container.size(); i ++){
        item = new QTableWidgetItem(container.cont.at(i).Name);
        ui->tableWidget->setItem(i, 0, item);

        item = new QTableWidgetItem(container.cont.at(i).Kind);
        ui->tableWidget->setItem(i, 1, item);

        item = new QTableWidgetItem(container.cont.at(i).Price);
        ui->tableWidget->setItem(i, 2, item);

        item = new QTableWidgetItem(QString::number(container.cont.at(i).Amount));
        ui->tableWidget->setItem(i, 3, item);
    }

}
