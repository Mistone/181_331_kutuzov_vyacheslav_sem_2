#ifndef USER_MAIN_WINDOW_H
#define USER_MAIN_WINDOW_H

#include <QDialog>
#include "data.h"
#include "client.h"
#include "user_medicine_window.h"
#include "user_cart.h"
#include "history_cart_window.h"

namespace Ui {
class User_Main_Window;
}

class User_Main_Window : public QDialog
{
    Q_OBJECT

public:
    explicit User_Main_Window(Data *DB = nullptr, Client *_client = nullptr);
    ~User_Main_Window();

private slots:
    void on_Medicine_List_clicked();

    void on_My_List_clicked();

private:
    Ui::User_Main_Window *ui;
    Data *DB;
    Client *client;
};

#endif // USER_MAIN_WINDOW_H
