#ifndef USER_CART_H
#define USER_CART_H

#include <QDialog>
#include "data.h"
#include "client.h"

namespace Ui {
class User_Cart;
}

class User_Cart : public QDialog
{
    Q_OBJECT

public:
    explicit User_Cart(Data *_DB = nullptr, Client *_client = nullptr);
    ~User_Cart();

private:
    Ui::User_Cart *ui;
    Data *DB;
    Client *Client_;
};

#endif // USER_CART_H
