/********************************************************************************
** Form generated from reading UI file 'user_medicine_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USER_MEDICINE_WINDOW_H
#define UI_USER_MEDICINE_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_User_Medicine_Window
{
public:
    QTableWidget *tableWidget;
    QComboBox *comboBox;
    QLineEdit *lineEdit;
    QPushButton *pushButton;

    void setupUi(QDialog *User_Medicine_Window)
    {
        if (User_Medicine_Window->objectName().isEmpty())
            User_Medicine_Window->setObjectName(QString::fromUtf8("User_Medicine_Window"));
        User_Medicine_Window->resize(813, 508);
        tableWidget = new QTableWidget(User_Medicine_Window);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 130, 821, 381));
        comboBox = new QComboBox(User_Medicine_Window);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(30, 40, 161, 24));
        lineEdit = new QLineEdit(User_Medicine_Window);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(210, 40, 281, 24));
        pushButton = new QPushButton(User_Medicine_Window);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(510, 40, 141, 25));

        retranslateUi(User_Medicine_Window);

        QMetaObject::connectSlotsByName(User_Medicine_Window);
    } // setupUi

    void retranslateUi(QDialog *User_Medicine_Window)
    {
        User_Medicine_Window->setWindowTitle(QApplication::translate("User_Medicine_Window", "Dialog", nullptr));
        pushButton->setText(QApplication::translate("User_Medicine_Window", "Search", nullptr));
    } // retranslateUi

};

namespace Ui {
    class User_Medicine_Window: public Ui_User_Medicine_Window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USER_MEDICINE_WINDOW_H
