#include "user_list_window.h"
#include "ui_user_list_window.h"

user_list_window::user_list_window(Data *_DB) :
    ui(new Ui::user_list_window),
    DB(_DB)
{
    ui->setupUi(this);

    ui->tableWidget->setRowCount(DB->Clients.size());
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Логин"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Пароль"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("ФИО"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Номер телефона"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i < DB->Clients.size(); i ++){
        item = new QTableWidgetItem(DB->Clients.cont.at(i).Login);
        ui->tableWidget->setItem(i, 0, item);

        item = new QTableWidgetItem(DB->Clients.cont.at(i).Password);
        ui->tableWidget->setItem(i, 1, item);

        item = new QTableWidgetItem(DB->Clients.cont.at(i).Fio);
        ui->tableWidget->setItem(i, 2, item);

        item = new QTableWidgetItem(DB->Clients.cont.at(i).Phone_number);
        ui->tableWidget->setItem(i, 3, item);
    }
}

user_list_window::~user_list_window()
{
    delete ui;
}

void user_list_window::on_tableWidget_cellChanged(int row, int column)
{
    if(DB->level_of_access == "admin"){
        if(column == 0)
            DB->Clients.cont.at(row).Login = ui->tableWidget->item(row, column)->text();
        else if(column == 1)
            DB->Clients.cont.at(row).Password = ui->tableWidget->item(row, column)->text();
        else if (column == 2)
            DB->Clients.cont.at(row).Fio = ui->tableWidget->item(row, column)->text();
        else if(column == 3)
            DB->Clients.cont.at(row).Phone_number = ui->tableWidget->item(row, column)->text();
        DB->upload_users();
    }
}
