#ifndef _CHAT_H
#define _CHAT_H

#include <QDialog>
#include "user.h"

namespace Ui {
class _chat;
}

class _chat : public QDialog
{
    Q_OBJECT

public:
    explicit _chat(QWidget *parent = nullptr, user *user = nullptr);
    ~_chat();

private slots:
    void on_send_clicked();

public slots:
    void table_update(QTableWidget *table);
private:
    Ui::_chat *ui;
    user *_user;
};



#endif // _CHAT_H
