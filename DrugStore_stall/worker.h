#ifndef WORKER_H
#define WORKER_H

#include "people.h"


class Worker : public People
{
public:
    Worker();
    Worker(QString login, QString password, QString FIO, QString phone_number);
    ~Worker();

    //char buy_list_all_people();
};

#endif // WORKER_H
