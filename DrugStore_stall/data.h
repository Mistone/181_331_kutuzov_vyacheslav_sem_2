#ifndef DATA_H
#define DATA_H
#include <QString>
#include "container.h"
#include "client.h"
#include "people.h"
#include "admin.h"
#include "worker.h"
#include "medicines.h"
#include "history_cart.h"
//#include "user_main_window.h"


class Data
{
public:
    QString level_of_access = "user";
    QString error;

    Container<Admin> Admins;
    Container<Client> Clients;
    Container<Worker> Workers;
    Container<Medicines> Drug;
    Container<history_cart> History_cart;


    Data();
    ~Data();

    void download_all();
    void upload_all();


    Container<Client> download_users();
    void upload_users();

    Container<Admin> download_admins();
    void upload_admins();

    Container<Worker> download_workers();
    void upload_workers();

    Container<Medicines> download_drug();
    void upload_drug();

    Container<history_cart> download_history_cart();
    void upload_history_cart();


    template <class T>
    void reg(T user, QString lvl_of_access);

    void login(People auth, QString type);
};

#endif // DATA_H
