#include "user.h"

#include <QTcpSocket>
#include <QTextEdit>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QDateTime>
#include <QCloseEvent>
#include <QMessageBox>
#include <openssl/evp.h>
#include <openssl/aes.h>
#include <QBuffer>
#include <QTextCodec>
#include "encrypt.h"
#include <QMessageBox>
#include "chat.h"
#include "_chat.h"
#include <vector>
#include <QHeaderView>

user::user(const QString &strHost, quint16 port):
    _nextBlockSize(0),
    _login("")
{
    _tcpSocket = new QTcpSocket(this);
    _tcpSocket->connectToHost(strHost, port);

    connect(_tcpSocket, SIGNAL(connected()), this, SLOT(slotConnected()));
    connect(_tcpSocket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
}

void user::slotReadyRead(){
    QDataStream in(_tcpSocket);
    in.setVersion(QDataStream::Qt_5_10);

    while(true){

        if (_nextBlockSize == 0){
            if (_tcpSocket->bytesAvailable() <static_cast<int>(sizeof(quint16))){
                break;
            }

            in >> _nextBlockSize;
        }

        if (_tcpSocket->bytesAvailable() < _nextBlockSize){
            break;
        }

        QByteArray state;
        QString message;
        QByteArray message_ba;
        QByteArray type;

        in >> state >> type;
        _nextBlockSize = 0;

        if (QString(state) == "err"){
            in >> message;
            QMessageBox::warning(this, "Внимание", message);
        } else if (QString(state) == "suc") {
            if (QString(type) == "auth" || QString(type) == "signup"){
                connect(this, SIGNAL(success_auth()), _cur_win, SLOT(change_ui()));
                emit success_auth();
                connect(this, SIGNAL(table_update(QTableWidget*)), _cur_win, SLOT(table_update(QTableWidget*)));
            } else if (QString(type) == "mess"){
                this->slotSendToServer("<<<get_messages>>> ");
                _nextBlockSize = 0;
            } else if (QString(type) == "get"){
                in >> message_ba;
                encrypt encryption(encrypt::AES_128, encrypt::ECB);
                message = QString::fromUtf8(encryption.decode(message_ba, "0123456789abcdeF"));
                QStringList messages = message.split("////");
                QStringList values;
                _table.setRowCount(messages.size()-1);
                _table.setColumnCount(3);
                _table.setHorizontalHeaderItem(0, new QTableWidgetItem("Наименование"));
                _table.setHorizontalHeaderItem(1, new QTableWidgetItem("Цена"));
                _table.setHorizontalHeaderItem(2, new QTableWidgetItem("Количество на складе"));
                QTableWidgetItem *item;
                for (int i = 0; i < messages.size() - 1; i ++){
                    values = messages[i].split("(*)(*)(*)");
                    for (int j = 0; j < 3; j ++){
                        QString val = values[j];
                        item = new QTableWidgetItem(val);
                        item->setFlags(item->flags() ^ Qt::ItemIsEditable);
                        _table.setItem(i, j, item);
                    }
                }
                _table.resizeRowsToContents();
                _table.resizeColumnsToContents();
                _table.horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
                _table.verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
                emit table_update(&_table);
            } else {
                in >> message;
                QMessageBox::information(this, "Внимание", message);
            }
        }

    }
}

void user::slotSendToServer(QString to_send){
    QString to_server;
    if (to_send.split(" ")[0] == "<<<auth>>>" || to_send.split(" ")[0] == "<<<signup>>>"){
        encrypt encryption(encrypt::AES_128, encrypt::ECB);
        QByteArray login_enc = encryption.encode(to_send.split(" ")[1].toLatin1(), "0123456789abcdeF");
        QByteArray password_enc = sha256(to_send.split(" ")[2].toLatin1());
        QByteArray  arrBlock;
        QDataStream out(&arrBlock, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_10);
        if (to_send.split(" ")[0] == "<<<auth>>>"){
            out << quint16(0) << "<<<auth>>>"<< login_enc << password_enc;
        } else {
            out << quint16(0) << "<<<signup>>>"<< login_enc << password_enc;
        }
        out.device()->seek(0);
        out<< quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

        _tcpSocket->write(arrBlock);
    } else if (to_send.split(" ")[0] == "<<<get_messages>>>"){
        QByteArray arrBlock;
        QDataStream out(&arrBlock, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_10);
        out << quint16(0) << "<<<get_messages>>>";
        out.device()->seek(0);
        out<< quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

        _tcpSocket->write(arrBlock);
    }
    else  if (to_send.split("::")[0] == "<<<message>>>"){
        encrypt encryption(encrypt::AES_128, encrypt::ECB);
        QByteArray Name_enc = encryption.encode(to_send.split("::")[1].toLatin1(), "0123456789abcdeF");
        QByteArray Price_enc = encryption.encode(to_send.split("::")[2].toLatin1(), "0123456789abcdeF");
        QByteArray Amount_enc = encryption.encode(to_send.split("::")[3].toLatin1(), "0123456789abcdeF");
        QByteArray arrBlock;
        QDataStream out(&arrBlock, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_10);
        out << quint16(0) << "<<<message>>>"<< Name_enc << Price_enc << Amount_enc;
        out.device()->seek(0);
        out<< quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

        _tcpSocket->write(arrBlock);
    }
     else  if (to_send.split("::")[0] == "<<<message_delete>>>"){
         encrypt encryption(encrypt::AES_128, encrypt::ECB);
         QByteArray Name_enc = encryption.encode(to_send.split("::")[1].toLatin1(), "0123456789abcdeF");
         QByteArray Price_enc = encryption.encode(to_send.split("::")[2].toLatin1(), "0123456789abcdeF");
         QByteArray Amount_enc = encryption.encode(to_send.split("::")[3].toLatin1(), "0123456789abcdeF");
         QByteArray arrBlock;
         QDataStream out(&arrBlock, QIODevice::WriteOnly);
         out.setVersion(QDataStream::Qt_5_10);
         out << quint16(0) << "<<<message_delete>>>"<< Name_enc << Price_enc << Amount_enc;
         out.device()->seek(0);
         out<< quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

         _tcpSocket->write(arrBlock);
    } else  if (to_send.split(" ")[0] == "<<<find>>>"){
        encrypt encryption(encrypt::AES_128, encrypt::ECB);
        QByteArray to_find_enc = encryption.encode(to_send.split(" ")[1].toLatin1(), "0123456789abcdeF");
        QByteArray category_enc = encryption.encode(to_send.split(" ")[2].toLatin1(), "0123456789abcdeF");
        QByteArray arrBlock;
        QDataStream out(&arrBlock, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_10);
        out << quint16(0) << "<<<find>>>"<< to_find_enc << category_enc << QDateTime::currentDateTime();
        out.device()->seek(0);
        out<< quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

        _tcpSocket->write(arrBlock);
    }

}

void user::slotConnected(){
    qDebug() << "ok";
}

QByteArray user::sha256(const QByteArray& text)
{
    unsigned int outLen = 0;
    QByteArray dataBuff;
    dataBuff.resize(EVP_MAX_MD_SIZE);
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    EVP_MD_CTX* evpMdCtx = EVP_MD_CTX_create();
#else
    EVP_MD_CTX* evpMdCtx = EVP_MD_CTX_new();
#endif
    EVP_DigestInit(evpMdCtx, EVP_sha256());
    EVP_DigestUpdate(evpMdCtx, text.data(), static_cast<size_t>(text.size()));
    EVP_DigestFinal_ex(evpMdCtx, reinterpret_cast<unsigned char*>(dataBuff.data()), &outLen);
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    EVP_MD_CTX_cleanup(evpMdCtx);
    OPENSSL_free(evpMdCtx);
#else
    EVP_MD_CTX_free(evpMdCtx);
#endif
    dataBuff.resize(static_cast<int>(outLen));
    return dataBuff.toHex();
}

