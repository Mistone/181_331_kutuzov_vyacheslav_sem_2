#include "worker_main_window.h"
#include "ui_worker_main_window.h"


worker_main_window::worker_main_window(Data *_DB, Worker *_worker) :
    ui(new Ui::worker_main_window),
    DB(_DB),
    worker(_worker)
{
    ui->setupUi(this);
}

worker_main_window::~worker_main_window()
{
    delete ui;
}

void worker_main_window::on_All_Medicine_clicked()
{
    User_Medicine_Window _Medicines(DB);
    _Medicines.setModal(true);
    _Medicines.exec();
}

void worker_main_window::on_pushButton_clicked()
{
    history_cart_window _Cart(DB);
    _Cart.setModal(true);
    _Cart.exec();
}
