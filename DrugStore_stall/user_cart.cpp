#include "user_cart.h"
#include "ui_user_cart.h"

User_Cart::User_Cart(Data *_DB, Client *_client) :
    ui(new Ui::User_Cart),
    DB(_DB),
    Client_(_client)
    {
    ui->setupUi(this);


    int _count = 0;
    for (unsigned int i = 0; i < DB->History_cart.size(); i ++){
        if(DB->History_cart.cont.at(i).FIO == Client_->Fio){
            _count += 1;
        }
    }
    ui->tableWidget->setRowCount(_count);
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("ФИО"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Препарат"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Кол-во"));
    QTableWidgetItem *item;
    int count = 0;
    for (unsigned int i = 0; i < DB->History_cart.size(); i ++){
        if(DB->History_cart.cont.at(i).FIO == Client_->Fio){
            item = new QTableWidgetItem(DB->History_cart.cont.at(i).FIO);
            ui->tableWidget->setItem(count, 0, item);

            item = new QTableWidgetItem(DB->History_cart.cont.at(i).Medicine);
            ui->tableWidget->setItem(count, 1, item);

            item = new QTableWidgetItem(DB->History_cart.cont.at(i).Count);
            ui->tableWidget->setItem(count, 2, item);
            count += 1;
        }
    }
}

User_Cart::~User_Cart()
{
    delete ui;
}
