#include "_chat.h"
#include "ui__chat.h"


_chat::_chat(QWidget *parent, user *user) :
    QDialog(parent),
    _user(user),
    ui(new Ui::_chat)
{
    ui->setupUi(this);
    _user->slotSendToServer("<<<get_messages>>> ");
}

_chat::~_chat()
{
    delete ui;
}

void _chat::on_send_clicked()
{
    QString text = ui->text->text();
    _user->slotSendToServer("<<<message>>>::" + text);
}

void _chat::table_update(QTableWidget *table){
    qDebug() << "got table";
    ui->messages = table;
}

