#ifndef HISTORY_CART_WINDOW_H
#define HISTORY_CART_WINDOW_H

#include <QDialog>
#include "data.h"

namespace Ui {
class history_cart_window;
}

class history_cart_window : public QDialog
{
    Q_OBJECT

public:
    explicit history_cart_window(Data *_DB = nullptr);
    ~history_cart_window();

private slots:
    void on_tableWidget_cellChanged(int row, int column);

private:
    Ui::history_cart_window *ui;
    Data *DB;
};

#endif // HISTORY_CART_WINDOW_H
