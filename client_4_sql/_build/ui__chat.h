/********************************************************************************
** Form generated from reading UI file '_chat.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI__CHAT_H
#define UI__CHAT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui__chat
{
public:
    QTableWidget *messages;
    QLineEdit *text;
    QPushButton *send;

    void setupUi(QDialog *_chat)
    {
        if (_chat->objectName().isEmpty())
            _chat->setObjectName(QString::fromUtf8("_chat"));
        _chat->resize(400, 300);
        messages = new QTableWidget(_chat);
        messages->setObjectName(QString::fromUtf8("messages"));
        messages->setGeometry(QRect(0, 70, 401, 221));
        text = new QLineEdit(_chat);
        text->setObjectName(QString::fromUtf8("text"));
        text->setGeometry(QRect(10, 20, 231, 24));
        send = new QPushButton(_chat);
        send->setObjectName(QString::fromUtf8("send"));
        send->setGeometry(QRect(280, 20, 80, 25));

        retranslateUi(_chat);

        QMetaObject::connectSlotsByName(_chat);
    } // setupUi

    void retranslateUi(QDialog *_chat)
    {
        _chat->setWindowTitle(QApplication::translate("_chat", "Dialog", nullptr));
        send->setText(QApplication::translate("_chat", "send", nullptr));
    } // retranslateUi

};

namespace Ui {
    class _chat: public Ui__chat {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI__CHAT_H
