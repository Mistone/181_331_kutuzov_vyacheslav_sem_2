/********************************************************************************
** Form generated from reading UI file 'worker_list_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WORKER_LIST_WINDOW_H
#define UI_WORKER_LIST_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_Worker_list_window
{
public:
    QTableWidget *tableWidget;

    void setupUi(QDialog *Worker_list_window)
    {
        if (Worker_list_window->objectName().isEmpty())
            Worker_list_window->setObjectName(QString::fromUtf8("Worker_list_window"));
        Worker_list_window->resize(784, 490);
        tableWidget = new QTableWidget(Worker_list_window);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 0, 781, 481));

        retranslateUi(Worker_list_window);

        QMetaObject::connectSlotsByName(Worker_list_window);
    } // setupUi

    void retranslateUi(QDialog *Worker_list_window)
    {
        Worker_list_window->setWindowTitle(QApplication::translate("Worker_list_window", "Dialog", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Worker_list_window: public Ui_Worker_list_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WORKER_LIST_WINDOW_H
