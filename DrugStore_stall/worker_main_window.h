#ifndef WORKER_MAIN_WINDOW_H
#define WORKER_MAIN_WINDOW_H

#include <QDialog>
#include "data.h"
#include "user_medicine_window.h"
#include "history_cart_window.h"

namespace Ui {
class worker_main_window;
}

class worker_main_window : public QDialog
{
    Q_OBJECT

public:
    explicit worker_main_window(Data *DB = nullptr, Worker *_worker = nullptr);
    ~worker_main_window();

private slots:
    void on_All_Medicine_clicked();

    void on_pushButton_clicked();

private:
    Ui::worker_main_window *ui;
    Data *DB;
    Worker *worker;
};

#endif // WORKER_MAIN_WINDOW_H
