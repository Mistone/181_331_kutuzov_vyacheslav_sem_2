/********************************************************************************
** Form generated from reading UI file 'reg_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REG_WINDOW_H
#define UI_REG_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Reg_window
{
public:
    QPushButton *Registration;
    QGroupBox *groupBox;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *Login;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *Password;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QLineEdit *FIO;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QLineEdit *Phone_number;

    void setupUi(QDialog *Reg_window)
    {
        if (Reg_window->objectName().isEmpty())
            Reg_window->setObjectName(QString::fromUtf8("Reg_window"));
        Reg_window->resize(801, 496);
        Registration = new QPushButton(Reg_window);
        Registration->setObjectName(QString::fromUtf8("Registration"));
        Registration->setGeometry(QRect(160, 400, 511, 61));
        groupBox = new QGroupBox(Reg_window);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(130, 80, 551, 311));
        widget = new QWidget(groupBox);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(20, 40, 521, 251));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        Login = new QLineEdit(widget);
        Login->setObjectName(QString::fromUtf8("Login"));

        horizontalLayout->addWidget(Login);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        Password = new QLineEdit(widget);
        Password->setObjectName(QString::fromUtf8("Password"));

        horizontalLayout_2->addWidget(Password);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        horizontalLayout_3->addWidget(label_3);

        FIO = new QLineEdit(widget);
        FIO->setObjectName(QString::fromUtf8("FIO"));

        horizontalLayout_3->addWidget(FIO);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_4->addWidget(label_4);

        Phone_number = new QLineEdit(widget);
        Phone_number->setObjectName(QString::fromUtf8("Phone_number"));

        horizontalLayout_4->addWidget(Phone_number);


        verticalLayout->addLayout(horizontalLayout_4);


        retranslateUi(Reg_window);

        QMetaObject::connectSlotsByName(Reg_window);
    } // setupUi

    void retranslateUi(QDialog *Reg_window)
    {
        Reg_window->setWindowTitle(QApplication::translate("Reg_window", "Dialog", nullptr));
        Registration->setText(QApplication::translate("Reg_window", "\320\240\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\321\217", nullptr));
        groupBox->setTitle(QApplication::translate("Reg_window", "\320\240\320\265\320\263\320\270\321\201\321\202\321\200\320\260\321\206\320\270\321\217", nullptr));
        label->setText(QApplication::translate("Reg_window", "\320\233\320\276\320\263\320\270\320\275", nullptr));
        label_2->setText(QApplication::translate("Reg_window", "\320\237\320\260\321\200\320\276\320\273\321\214", nullptr));
        label_3->setText(QApplication::translate("Reg_window", "\320\244\320\230\320\236", nullptr));
        label_4->setText(QApplication::translate("Reg_window", "\320\235\320\276\320\274\320\265\321\200", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Reg_window: public Ui_Reg_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REG_WINDOW_H
