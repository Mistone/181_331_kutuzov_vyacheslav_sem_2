#ifndef MEDICINES_H
#define MEDICINES_H

#include <QString>


class Medicines
{
public:
    Medicines();
    Medicines(QString name, QString kind, QString price, int amount);
    ~Medicines();


    //int Id;
    QString Name;
    QString Kind;
    QString Price;
    int Amount;
};


struct medicines{ //ToDo посоветоваться, нужна ли эта структура (я от нее отказался, но пока не стал убирать). Если нет, то стереть и забыть
    int id;
    QString name;
    QString kind;
    QString price;
    int amount;
};

#endif // MEDICINES_H
