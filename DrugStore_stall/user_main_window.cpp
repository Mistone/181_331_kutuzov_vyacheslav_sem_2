#include "user_main_window.h"
#include "ui_user_main_window.h"

User_Main_Window::User_Main_Window(Data *_DB, Client *_client) :
    ui(new Ui::User_Main_Window),
    DB(_DB),
    client(_client)
{
    ui->setupUi(this);
}

User_Main_Window::~User_Main_Window()
{
    delete ui;
}

void User_Main_Window::on_Medicine_List_clicked()
{
    User_Medicine_Window _Medicines(DB);
    _Medicines.setModal(true);
    _Medicines.exec();
}

void User_Main_Window::on_My_List_clicked()
{
    User_Cart _Cart(DB, client);
    _Cart.setModal(true);
    _Cart.exec();
}
