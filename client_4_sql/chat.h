#ifndef CHAT_H
#define CHAT_H

#include <QWidget>
#include "user.h"

namespace Ui {
class chat;
}

class chat : public QWidget
{
    Q_OBJECT

public:
    explicit chat(user * user);
    ~chat();
    Ui::chat *ui;
private:

    user *_user;

};


#endif // CHAT_H
