/********************************************************************************
** Form generated from reading UI file 'user_list_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USER_LIST_WINDOW_H
#define UI_USER_LIST_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_user_list_window
{
public:
    QTableWidget *tableWidget;

    void setupUi(QDialog *user_list_window)
    {
        if (user_list_window->objectName().isEmpty())
            user_list_window->setObjectName(QString::fromUtf8("user_list_window"));
        user_list_window->resize(765, 475);
        tableWidget = new QTableWidget(user_list_window);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(0, 0, 761, 471));

        retranslateUi(user_list_window);

        QMetaObject::connectSlotsByName(user_list_window);
    } // setupUi

    void retranslateUi(QDialog *user_list_window)
    {
        user_list_window->setWindowTitle(QApplication::translate("user_list_window", "Dialog", nullptr));
    } // retranslateUi

};

namespace Ui {
    class user_list_window: public Ui_user_list_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USER_LIST_WINDOW_H
