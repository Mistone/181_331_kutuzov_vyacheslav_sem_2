#ifndef HISTORY_CART_H
#define HISTORY_CART_H
#include <QString>

class history_cart
{
public:
    history_cart();
    history_cart(QString Fio, QString medicine, QString count);
    ~history_cart();

    QString FIO;
    QString Medicine;
    QString Count;
};

#endif // HISTORY_CART_H
