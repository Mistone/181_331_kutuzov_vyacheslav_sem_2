#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "reg_window.h"
#include "login_window.h"
#include "data.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_Reg_clicked();

    void on_Auth_clicked();

private:
    Ui::MainWindow *ui;
    Reg_window *reg_window;
    login_window *log_window;
};

#endif // MAINWINDOW_H
