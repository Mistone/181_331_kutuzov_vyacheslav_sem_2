#ifndef CONTAINER_H
#define CONTAINER_H

#include <vector>

template <class T>
class Container
{
public:
    std::vector<T> cont;

    Container(){

    }

    Container(T element){
        cont.push_back(element);
    }

    void add(T element){
        cont.push_back(element);
    }

    unsigned long long size(){
        return cont.size();
    }

    ~Container(){

    }

};

#endif // CONTAINER_H
