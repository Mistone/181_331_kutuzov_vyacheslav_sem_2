#ifndef PEOPLE_H
#define PEOPLE_H

#include <QString>


class People
{
public:
    People();
    People(QString login, QString password, QString FIO, QString phone_number);
    ~People();

    QString Login;
    QString Password;
    QString Fio;
    QString Phone_number;
};

#endif // PEOPLE_H
