/********************************************************************************
** Form generated from reading UI file 'user_cart.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USER_CART_H
#define UI_USER_CART_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_User_Cart
{
public:
    QTableWidget *tableWidget;

    void setupUi(QDialog *User_Cart)
    {
        if (User_Cart->objectName().isEmpty())
            User_Cart->setObjectName(QString::fromUtf8("User_Cart"));
        User_Cart->resize(762, 520);
        tableWidget = new QTableWidget(User_Cart);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(10, 10, 741, 491));

        retranslateUi(User_Cart);

        QMetaObject::connectSlotsByName(User_Cart);
    } // setupUi

    void retranslateUi(QDialog *User_Cart)
    {
        User_Cart->setWindowTitle(QApplication::translate("User_Cart", "Dialog", nullptr));
    } // retranslateUi

};

namespace Ui {
    class User_Cart: public Ui_User_Cart {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USER_CART_H
