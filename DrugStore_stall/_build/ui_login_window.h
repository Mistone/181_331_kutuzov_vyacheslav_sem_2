/********************************************************************************
** Form generated from reading UI file 'login_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGIN_WINDOW_H
#define UI_LOGIN_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_login_window
{
public:
    QGroupBox *groupBox;
    QPushButton *Join;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *Login;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *Password;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_3;
    QRadioButton *Admin;
    QRadioButton *User;
    QRadioButton *Worker;

    void setupUi(QDialog *login_window)
    {
        if (login_window->objectName().isEmpty())
            login_window->setObjectName(QString::fromUtf8("login_window"));
        login_window->resize(611, 439);
        groupBox = new QGroupBox(login_window);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(140, 70, 301, 311));
        Join = new QPushButton(groupBox);
        Join->setObjectName(QString::fromUtf8("Join"));
        Join->setGeometry(QRect(10, 240, 281, 51));
        layoutWidget = new QWidget(groupBox);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(10, 70, 281, 111));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        Login = new QLineEdit(layoutWidget);
        Login->setObjectName(QString::fromUtf8("Login"));

        horizontalLayout->addWidget(Login);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        Password = new QLineEdit(layoutWidget);
        Password->setObjectName(QString::fromUtf8("Password"));
        Password->setEchoMode(QLineEdit::Password);

        horizontalLayout_2->addWidget(Password);


        verticalLayout->addLayout(horizontalLayout_2);

        widget = new QWidget(groupBox);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setGeometry(QRect(10, 200, 281, 22));
        horizontalLayout_3 = new QHBoxLayout(widget);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        Admin = new QRadioButton(widget);
        Admin->setObjectName(QString::fromUtf8("Admin"));

        horizontalLayout_3->addWidget(Admin);

        User = new QRadioButton(widget);
        User->setObjectName(QString::fromUtf8("User"));

        horizontalLayout_3->addWidget(User);

        Worker = new QRadioButton(widget);
        Worker->setObjectName(QString::fromUtf8("Worker"));

        horizontalLayout_3->addWidget(Worker);


        retranslateUi(login_window);

        QMetaObject::connectSlotsByName(login_window);
    } // setupUi

    void retranslateUi(QDialog *login_window)
    {
        login_window->setWindowTitle(QApplication::translate("login_window", "Dialog", nullptr));
        groupBox->setTitle(QApplication::translate("login_window", "\320\220\320\262\321\202\320\276\321\200\320\270\320\267\320\260\321\206\320\270\321\217", nullptr));
        Join->setText(QApplication::translate("login_window", "\320\222\320\276\320\271\321\202\320\270", nullptr));
        label->setText(QApplication::translate("login_window", "\320\233\320\276\320\263\320\270\320\275", nullptr));
        label_2->setText(QApplication::translate("login_window", "\320\237\320\260\321\200\320\276\320\273\321\214", nullptr));
        Admin->setText(QApplication::translate("login_window", "Admin", nullptr));
        User->setText(QApplication::translate("login_window", "User", nullptr));
        Worker->setText(QApplication::translate("login_window", "Worker", nullptr));
    } // retranslateUi

};

namespace Ui {
    class login_window: public Ui_login_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGIN_WINDOW_H
