#-------------------------------------------------
#
# Project created by QtCreator 2019-03-10T16:29:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DrugStore_stall
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    data.cpp \
    people.cpp \
    admin.cpp \
    client.cpp \
    worker.cpp \
    medicines.cpp \
    reg_window.cpp \
    login_window.cpp \
    container.cpp \
    user_main_window.cpp \
    user_medicine_window.cpp \
    user_cart.cpp \
    worker_main_window.cpp \
    admin_main_window.cpp \
    worker_list_window.cpp \
    history_cart_window.cpp \
    history_cart.cpp \
    user_list_window.cpp

HEADERS += \
        mainwindow.h \
    data.h \
    people.h \
    admin.h \
    client.h \
    worker.h \
    medicines.h \
    reg_window.h \
    login_window.h \
    container.h \
    user_main_window.h \
    user_medicine_window.h \
    user_cart.h \
    worker_main_window.h \
    admin_main_window.h \
    worker_list_window.h \
    history_cart_window.h \
    history_cart.h \
    user_list_window.h

FORMS += \
        mainwindow.ui \
    reg_window.ui \
    login_window.ui \
    user_main_window.ui \
    user_medicine_window.ui \
    user_cart.ui \
    worker_main_window.ui \
    admin_main_window.ui \
    worker_list_window.ui \
    history_cart_window.ui \
    user_list_window.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
