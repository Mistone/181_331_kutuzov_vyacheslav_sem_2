#ifndef REG_WINDOW_H
#define REG_WINDOW_H

#include <QDialog>
#include <QMessageBox>
#include "data.h"

namespace Ui {
class Reg_window;
}

class Reg_window : public QDialog
{
    Q_OBJECT

public:
    explicit Reg_window(QWidget *parent = nullptr);
    ~Reg_window();

private slots:
    void on_Registration_clicked();
private:
    Ui::Reg_window *ui;
};

#endif // REG_WINDOW_H
