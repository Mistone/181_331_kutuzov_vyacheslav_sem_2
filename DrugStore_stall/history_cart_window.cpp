#include "history_cart_window.h"
#include "ui_history_cart_window.h"
#include <QFile>

history_cart_window::history_cart_window(Data *_DB) :
    ui(new Ui::history_cart_window),
    DB(_DB)
{
    ui->setupUi(this);


    ui->tableWidget->setRowCount(DB->History_cart.size());
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("ФИО"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Препарат"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("Кол-во"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i < DB->History_cart.size(); i ++){
        item = new QTableWidgetItem(DB->History_cart.cont.at(i).FIO);
        ui->tableWidget->setItem(i, 0, item);

        item = new QTableWidgetItem(DB->History_cart.cont.at(i).Medicine);
        ui->tableWidget->setItem(i, 1, item);

        item = new QTableWidgetItem(DB->History_cart.cont.at(i).Count);
        ui->tableWidget->setItem(i, 2, item);
    }
}

history_cart_window::~history_cart_window()
{
    delete ui;
}

void history_cart_window::on_tableWidget_cellChanged(int row, int column)
{
    if(DB->level_of_access == "admin"){
        if(column == 0)
            DB->History_cart.cont.at(row).FIO = ui->tableWidget->item(row, column)->text();
        else if(column == 1)
            DB->History_cart.cont.at(row).Medicine = ui->tableWidget->item(row, column)->text();
        else if (column == 2)
            DB->History_cart.cont.at(row).Count = ui->tableWidget->item(row, column)->text();
        DB->upload_history_cart();
    }
}
