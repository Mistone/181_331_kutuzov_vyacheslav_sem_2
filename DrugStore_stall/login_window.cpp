#include "login_window.h"
#include "ui_login_window.h"

login_window::login_window(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::login_window)
{
    ui->setupUi(this);
}

login_window::~login_window()
{
    delete ui;
}

void login_window::on_Join_clicked()
{
    QString login = ui->Login->text();
    QString password = ui->Password->text();
    People people;
    people.Login = login;
    people.Password = password;
    Data *DB = new Data;
    DB->download_all();
    if (ui->Admin->isChecked())
        DB->login(people, "Admin");

    if (ui->Worker->isChecked())
        DB->login(people, "Worker");

    if (ui->User->isChecked())
        DB->login(people, "User");
}
