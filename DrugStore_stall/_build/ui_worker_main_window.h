/********************************************************************************
** Form generated from reading UI file 'worker_main_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WORKER_MAIN_WINDOW_H
#define UI_WORKER_MAIN_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_worker_main_window
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *All_Medicine;
    QPushButton *pushButton;

    void setupUi(QDialog *worker_main_window)
    {
        if (worker_main_window->objectName().isEmpty())
            worker_main_window->setObjectName(QString::fromUtf8("worker_main_window"));
        worker_main_window->resize(846, 498);
        verticalLayoutWidget = new QWidget(worker_main_window);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(160, 100, 531, 281));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        All_Medicine = new QPushButton(verticalLayoutWidget);
        All_Medicine->setObjectName(QString::fromUtf8("All_Medicine"));

        verticalLayout->addWidget(All_Medicine);

        pushButton = new QPushButton(verticalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);


        retranslateUi(worker_main_window);

        QMetaObject::connectSlotsByName(worker_main_window);
    } // setupUi

    void retranslateUi(QDialog *worker_main_window)
    {
        worker_main_window->setWindowTitle(QApplication::translate("worker_main_window", "Dialog", nullptr));
        All_Medicine->setText(QApplication::translate("worker_main_window", "\320\241\320\277\320\270\321\201\320\276\320\272 \320\274\320\265\320\264\320\270\320\272\320\260\320\274\320\265\320\275\321\202\320\276\320\262", nullptr));
        pushButton->setText(QApplication::translate("worker_main_window", "\320\230\321\201\321\202\320\276\321\200\320\270\321\217 \320\277\320\276\320\272\321\203\320\277\320\276\320\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class worker_main_window: public Ui_worker_main_window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WORKER_MAIN_WINDOW_H
