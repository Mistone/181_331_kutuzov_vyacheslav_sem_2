#ifndef ADMIN_MAIN_WINDOW_H
#define ADMIN_MAIN_WINDOW_H

#include <QDialog>
#include "data.h"
#include "worker_list_window.h"
#include "history_cart_window.h"
#include "user_medicine_window.h"
#include "user_list_window.h"

namespace Ui {
class Admin_Main_Window;
}

class Admin_Main_Window : public QDialog
{
    Q_OBJECT

public:
    explicit Admin_Main_Window(Data *DB = nullptr, Admin *_admin = nullptr);
    ~Admin_Main_Window();

private slots:
    void on_Worker_list_clicked();

    void on_pushButton_clicked();

    void on_User_list_clicked();

    void on_Medicine_list_clicked();

private:
    Ui::Admin_Main_Window *ui;
    Data *DB;
    Admin *admin;
};

#endif // ADMIN_MAIN_WINDOW_H
