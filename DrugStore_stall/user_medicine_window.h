#ifndef USER_MEDICINE_WINDOW_H
#define USER_MEDICINE_WINDOW_H

#include <QDialog>
#include <QObject>
#include "data.h"

namespace Ui {
class User_Medicine_Window;
}

class User_Medicine_Window : public QDialog
{
    Q_OBJECT

public:
    explicit User_Medicine_Window(Data *_DB = nullptr);
    ~User_Medicine_Window();

private slots:
    void on_tableWidget_cellChanged(int row, int column);

    void on_pushButton_clicked();

private:
    Ui::User_Medicine_Window *ui;
    Data *DB;
};

#endif // USER_MEDICINE_WINDOW_H
