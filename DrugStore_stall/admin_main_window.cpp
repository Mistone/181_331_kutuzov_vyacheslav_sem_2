#include "admin_main_window.h"
#include "ui_admin_main_window.h"

Admin_Main_Window::Admin_Main_Window(Data *_DB, Admin *_admin) :
    ui(new Ui::Admin_Main_Window),
    DB(_DB),
    admin(_admin)
{
    ui->setupUi(this);
}

Admin_Main_Window::~Admin_Main_Window()
{
    delete ui;
}

void Admin_Main_Window::on_Worker_list_clicked()
{
    Worker_list_window _Worker_list(DB);
    _Worker_list.setModal(true);
    _Worker_list.exec();
}

void Admin_Main_Window::on_pushButton_clicked()
{
    history_cart_window _Cart(DB);
    _Cart.setModal(true);
    _Cart.exec();
}

void Admin_Main_Window::on_User_list_clicked()
{
    user_list_window _Users(DB);
    _Users.setModal(true);
    _Users.exec();
}

void Admin_Main_Window::on_Medicine_list_clicked()
{
    User_Medicine_Window _Medicine(DB);
    _Medicine.setModal(true);
    _Medicine.exec();
}
