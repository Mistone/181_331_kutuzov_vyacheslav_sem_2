#include "authorization.h"
#include "ui_authorization.h"
#include "QComboBox"
#include <QMessageBox>

authorization::authorization(user *user) :
    ui(new Ui::authorization),
    _user(user)
{
    ui->setupUi(this);
    _user->_cur_win = this;
    this->setAttribute( Qt::WA_DeleteOnClose, false );
}

authorization::~authorization()
{
    delete ui;
}

void authorization::on_send_clicked()
{
    QString name = line->text();
    QString price = line1->text();
    QString amount = line2->text();
    _user->slotSendToServer("<<<message>>>::" + name + "::" + price + "::" + amount);
    line->setText("");
    line2->setText("");
    line1->setText("");
    QMessageBox::information(this, "Успешно", "Лекарство успешно добавлено");
}

void authorization::on_delete_clicked()
{
    QString name = line->text();
    QString price = line1->text();
    QString amount = line2->text();
    _user->slotSendToServer("<<<message_delete>>>::" + name + "::" + price + "::" + amount);
    line->setText("");
    line2->setText("");
    line1->setText("");
    QMessageBox::information(this, "Успешно", "Лекарство успешно удалено");
}

void authorization::change_ui(){
    ui->password->close();
    ui->login->close();
    ui->label->close();
    ui->label_2->close();
    ui->sign->close();
    ui->auth->close();
    layout_1 = new QVBoxLayout();
    QHBoxLayout *layout_2 = new QHBoxLayout();
    QHBoxLayout *layout_3 = new QHBoxLayout();
    line = new QLineEdit();
    line->setPlaceholderText("Введите название");
    line1 = new QLineEdit();
    line1->setPlaceholderText("Введите цену");
    line2 = new QLineEdit();
    line2->setPlaceholderText("Введите кол-во");
    QPushButton *button_1 = new QPushButton();
    QPushButton *button_2 = new QPushButton();
    QPushButton *button_3 = new QPushButton();
    connect(button_1, SIGNAL(clicked()), this, SLOT(on_send_clicked()));
    connect(button_3, SIGNAL(clicked()), this, SLOT(on_delete_clicked()));
    connect(button_2, SIGNAL(clicked()), this, SLOT(on_find_clicked()));
    button_1->setText("Добавить");
    button_3->setText("Удалить");
    button_2->setText("Найти");
    QLabel *label = new QLabel();
    QLabel *label_1 = new QLabel();
    label_1->setText("<H4>Поиск:</H4>");
    _table = new QTableWidget();
    _table->setRowCount(1);
    line_1 = new QLineEdit();
    choice = new QComboBox();
    choice->addItem("Name");
    choice->addItem("Price");
    choice->addItem("Amount");
    layout_3->addWidget(label_1);
    layout_3->addWidget(choice);
    layout_3->addWidget(line_1);
    layout_3->addWidget(button_2);
    label->setText("<H1>Лекарства</H1>");
    layout_1 ->addWidget(label);
    layout_2 ->addWidget(line);
    layout_2 ->addWidget(line1);
    layout_2 ->addWidget(line2);
    layout_2 ->addWidget(button_1);
    layout_2 ->addWidget(button_3);
    layout_1 ->addLayout(layout_2);
    layout_1 ->addLayout(layout_3);
    layout_1 ->addWidget(_table);
    setLayout(layout_1);
    resize(800,600);
    _user->slotSendToServer("<<<get_messages>>> ");
    this->setWindowTitle("Список лекарств");
}

void authorization::on_auth_clicked()
{
    QString login = ui->login->text();
    QString password = ui->password->text();
    _user->_login = login;
    _user->slotSendToServer("<<<auth>>> " + login + " " + password);
}

void authorization::on_sign_clicked()
{
    QString login = ui->login->text();
    QString password = ui->password->text();

    if (password == "" || login == ""){
        QMessageBox::warning(this, "Внимание", "Необходимо ввести логин и пароль!");
    } else {
        _user->_login = login;
        _user->slotSendToServer("<<<signup>>> " + login + " " + password);
    }

}

void authorization::table_update(QTableWidget *table){
    layout_1->removeWidget(_table);
    _table = table;
    layout_1->addWidget(_table);
}

void authorization::on_find_clicked(){
    QString to_find = line_1->text();
    QString category = choice->currentText();
    _user->slotSendToServer("<<<find>>> " + to_find + " " + category);
}

