#include "worker_list_window.h"
#include "ui_worker_list_window.h"

Worker_list_window::Worker_list_window(Data *_DB) :
    ui(new Ui::Worker_list_window),
    DB(_DB)
{
    ui->setupUi(this);

    ui->tableWidget->setRowCount(DB->Workers.size());
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setHorizontalHeaderItem(0, new QTableWidgetItem("Логин"));
    ui->tableWidget->setHorizontalHeaderItem(1, new QTableWidgetItem("Пароль"));
    ui->tableWidget->setHorizontalHeaderItem(2, new QTableWidgetItem("ФИО"));
    ui->tableWidget->setHorizontalHeaderItem(3, new QTableWidgetItem("Номер"));
    QTableWidgetItem *item;
    for (unsigned int i = 0; i < DB->Workers.size(); i ++){
        item = new QTableWidgetItem(DB->Workers.cont.at(i).Login);
        ui->tableWidget->setItem(i, 0, item);

        item = new QTableWidgetItem(DB->Workers.cont.at(i).Password);
        ui->tableWidget->setItem(i, 1, item);

        item = new QTableWidgetItem(DB->Workers.cont.at(i).Fio);
        ui->tableWidget->setItem(i, 2, item);

        item = new QTableWidgetItem(DB->Workers.cont.at(i).Phone_number);
        ui->tableWidget->setItem(i, 3, item);
    }
}

Worker_list_window::~Worker_list_window()
{
    delete ui;
}

void Worker_list_window::on_tableWidget_cellChanged(int row, int column)
{
    if(column == 0)
        DB->Workers.cont.at(row).Login = ui->tableWidget->item(row, column)->text();
    else if(column == 1)
        DB->Workers.cont.at(row).Password = ui->tableWidget->item(row, column)->text();
    else if (column == 2)
        DB->Workers.cont.at(row).Fio = ui->tableWidget->item(row, column)->text();
    else if(column == 3)
        DB->Workers.cont.at(row).Phone_number = ui->tableWidget->item(row, column)->text();
    DB->upload_workers();
}
