#ifndef ADMIN_H
#define ADMIN_H

#include "people.h"



class Admin : public People
{
public:
    Admin();
    Admin(QString login, QString password, QString FIO, QString phone_number);
    ~Admin();
};

#endif // ADMIN_H
