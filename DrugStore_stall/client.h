#ifndef CLIENT_H
#define CLIENT_H

#include "people.h"



class Client : public People
{
public:
    Client();
    Client(QString login, QString password, QString FIO, QString phone_number);
    ~Client();
};

#endif // CLIENT_H
