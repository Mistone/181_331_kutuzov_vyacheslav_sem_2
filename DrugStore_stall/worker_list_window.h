#ifndef WORKER_LIST_WINDOW_H
#define WORKER_LIST_WINDOW_H

#include <QDialog>
#include "data.h"

namespace Ui {
class Worker_list_window;
}

class Worker_list_window : public QDialog
{
    Q_OBJECT

public:
    explicit Worker_list_window(Data *_DB = nullptr);
    ~Worker_list_window();

private slots:
    void on_tableWidget_cellChanged(int row, int column);

private:
    Ui::Worker_list_window *ui;
    Data *DB;
};

#endif // WORKER_LIST_WINDOW_H
