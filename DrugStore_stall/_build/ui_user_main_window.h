/********************************************************************************
** Form generated from reading UI file 'user_main_window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_USER_MAIN_WINDOW_H
#define UI_USER_MAIN_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_User_Main_Window
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *Medicine_List;
    QPushButton *My_List;

    void setupUi(QDialog *User_Main_Window)
    {
        if (User_Main_Window->objectName().isEmpty())
            User_Main_Window->setObjectName(QString::fromUtf8("User_Main_Window"));
        User_Main_Window->resize(734, 535);
        verticalLayoutWidget = new QWidget(User_Main_Window);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(160, 70, 391, 361));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        Medicine_List = new QPushButton(verticalLayoutWidget);
        Medicine_List->setObjectName(QString::fromUtf8("Medicine_List"));

        verticalLayout->addWidget(Medicine_List);

        My_List = new QPushButton(verticalLayoutWidget);
        My_List->setObjectName(QString::fromUtf8("My_List"));

        verticalLayout->addWidget(My_List);


        retranslateUi(User_Main_Window);

        QMetaObject::connectSlotsByName(User_Main_Window);
    } // setupUi

    void retranslateUi(QDialog *User_Main_Window)
    {
        User_Main_Window->setWindowTitle(QApplication::translate("User_Main_Window", "Dialog", nullptr));
        Medicine_List->setText(QApplication::translate("User_Main_Window", "\320\241\320\277\320\270\321\201\320\276\320\272 \320\273\320\265\320\272\320\260\321\200\321\201\321\202\320\262", nullptr));
        My_List->setText(QApplication::translate("User_Main_Window", "\320\234\320\276\320\271 \321\201\320\277\320\270\321\201\320\276\320\272 \320\277\320\276\320\272\321\203\320\277\320\276\320\272", nullptr));
    } // retranslateUi

};

namespace Ui {
    class User_Main_Window: public Ui_User_Main_Window {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_USER_MAIN_WINDOW_H
