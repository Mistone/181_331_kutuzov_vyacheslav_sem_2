#include "server.h"

#include <QTcpServer>
#include <QTcpSocket>

#include <QTextEdit>
#include <QVBoxLayout>
#include <QLabel>

#include <QDataStream>
#include <QTime>

Server::Server(quint16 port): _nextBlockSize(0){
    _tcpServer = new QTcpServer(this);

    if (!_tcpServer->listen(QHostAddress::Any, port)){
        _tcpServer->close();
        return;
    }

    connect(_tcpServer, SIGNAL(newConnection()), this, SLOT(slotNewConnection()));

    _text = new QTextEdit();
    _text->setReadOnly(true);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(new QLabel("<H1>Сервер</H1>"));
    layout->addWidget(_text);
    setLayout(layout);
    resize(400,400);
}

void Server::slotNewConnection(){
    sockets.append(_tcpServer->nextPendingConnection());
    connect(sockets.last(),SIGNAL(readyRead()),this, SLOT(slotReadClient()));
    if (sockets.size() == 1) {
        gameState = 1;
        sendToClient(sockets.last(), "Поздравляю, вы играете Х-ком. Ждем второго игрока..");
    }
    else if (sockets.size() == 2) {
        gameState = 2;
        sendToClient(sockets.last(), "Поздравляю, вы играете О-ком. Ждите хода первого игрока..");
        sendToClient(sockets.at(0), "Второй игрок найден! Ход за вами");
    }
}

void Server::slotReadClient(){
    for (int i = 0; i < sockets.size(); i++)
    {
        QDataStream in(sockets.at(i));
        while(true)
        {
            if (_nextBlockSize == 0)
            {
                if (sockets.at(i)->bytesAvailable() <static_cast<int>(sizeof(quint16)))
                    break;
                in >> _nextBlockSize;
            }

            if (sockets.at(i)->bytesAvailable() < _nextBlockSize){
                break;
            }

            QTime time;
            QString str;
            in >> time >> str;

            if (gameState == 2){
                if (i == 0){
                    int i_k = str.split(' ')[0].toInt() % 3;
                    int j_k = str.split(' ')[1].toInt() % 3;
                    if (game[i_k][j_k] != "  ") {
                        QString message = time.toString() + " " + "Клиент отправил: " + str + ".";
                        _text->append(message);
                        _nextBlockSize = 0;
                        sendToClient(sockets.at(0), "Ячейка занята!");
                    }
                    else {
                        game[i_k][j_k] = "X";
                        if (isVictory()) {
                            game = {{"  ", "  ", "  "}, {"  ", "  ", "  "}, {"  ", "  ", "  "}};
                            _nextBlockSize = 0;
                            sendToClient(sockets.at(0), "С победой! Начнем новую игру? Твой ход первый");
                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "К сожалению, вы проиграли(( Но шанс отыграться еще есть, ждите первого хода второго игрока");
                        } else if (isEnd()) {
                            game = {{"  ", "  ", "  "}, {"  ", "  ", "  "}, {"  ", "  ", "  "}};
                            _nextBlockSize = 0;
                            sendToClient(sockets.at(0), "Ничья. Начнем новую игру, вы ходите первым");
                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "Ничья. Начнем новую игру, вы ходите вторым");
                        } else {
                            gameState = 3;
                            _nextBlockSize = 0;
                            sendToClient(sockets.at(0), "Отлично, ждите хода оппонента");
                            print(sockets.at(0));

                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "Сейчас ваш ход!");
                            print(sockets.last());
                        }
                    }
                } else if (i == 1) {
                    QString message = time.toString() + " " + "Клиент отправил: " + str + ".";
                    _text->append(message);
                    _nextBlockSize = 0;
                    sendToClient(sockets.last(), "Сейчас не ваш ход, подождите!");
                }
            } else if (gameState == 3) {
                if (str != ""){
                    if (i == 1){
                        int i_k = str.split(' ')[0].toInt();
                        int j_k = str.split(' ')[1].toInt();
                        if (game[i_k][j_k] != "  ") {
                            QString message = time.toString() + " " + "Клиент отправил: " + str + ".";
                            _text->append(message);
                            _nextBlockSize = 0;
                            sendToClient(sockets.last(), "Ячейка занята!");
                        }
                        else {
                            game[i_k][j_k] = "O";
                            if (isVictory()) {
                                gameState = 2;
                                game = {{"  ", "  ", "  "}, {"  ", "  ", "  "}, {"  ", "  ", "  "}};
                                _nextBlockSize = 0;
                                sendToClient(sockets.last(), "С победой! Игра окончена! Новая игра началась, ждите своего хода!");
                                _nextBlockSize = 0;
                                sendToClient(sockets.at(0), "Вы проиграли(( Начнем новую игру? Твой ход первый");
                            } else if (isEnd()) {
                                gameState = 2;
                                game = {{"  ", "  ", "  "}, {"  ", "  ", "  "}, {"  ", "  ", "  "}};
                                _nextBlockSize = 0;
                                sendToClient(sockets.at(0), "Ничья! Новая игра началась, ждите своего хода");
                                _nextBlockSize = 0;
                                sendToClient(sockets.last(), "Ничья! Новая игра началась, ваш ход- первый");
                            } else {
                                gameState = 2;


                                _nextBlockSize = 0;
                                sendToClient(sockets.last(), "Отлично, ждите хода оппонента");
                                print(sockets.last());
                                _nextBlockSize = 0;
                                sendToClient(sockets.at(0), "Сейчас ваш ход!");
                                print(sockets.at(0));
                            }
                        }
                    } else if (i == 0) {
                        QString message = time.toString() + " " + "Клиент отправил: " + str + ".";
                        _text->append(message);
                        _nextBlockSize = 0;
                        sendToClient(sockets.at(0), "Сейчас не ваш ход, подождите!");
                    }
                }

            } else if (gameState == 1){
                QString message = time.toString() + " " + "Клиент отправил: " + str + ".";
                _text->append(message);
                _nextBlockSize = 0;
                sendToClient(sockets.at(0), "Все еще ждем соперника..");
            }
        }

    }
}

bool Server::isVictory()
{
    for (unsigned int i = 0; i < 3; i++)
    {
        if (game[i][0] == game[i][1] && game[i][0] == game[i][2] && game[i][0] != "  ")
            return true;
    }
    for (unsigned int i = 0; i < 3; i++)
    {
        if (game[0][i] == game[1][i] && game[0][i] == game[2][i] && game[0][i] != "  ")
            return true;
    }
    if (game[0][0] == game[1][1] && game[1][1] == game[2][2] && game[0][0] != "  ")
        return true;
    if (game[0][2] == game[1][1] && game[1][1] == game[2][0] && game[1][1] != "  ")
        return true;
    return false;
}

bool Server::isEnd()
{
    if (!isVictory()){
        for (unsigned int i = 0; i < 3; i++){
            for (unsigned int j = 0; j < 3; j++){
                if (game[i][j] == "  ")
                    return false;
            }
        }
    } else return true;
    return true;
}

void Server::print(QTcpSocket * socket)
{
    for (unsigned int i = 0; i < 3; i++)
    {
        QString line;
        line = game[i][0] + "  " + game[i][1] + "  " + game[i][2];
        _nextBlockSize = 0;
        sendToClient(socket, line);
    }
}

void Server::sendToClient(QTcpSocket* socket, const QString &str){


    QByteArray arrBlock;
    QDataStream out(&arrBlock, QIODevice::WriteOnly);

    out << quint16(0) << QTime::currentTime() << str;
    out.device()->seek(0);
    out << quint16(arrBlock.size() - static_cast<int>(sizeof(quint16)));

    socket->write(arrBlock);

}

void Server::change_state(){
    if (state)
        state = false;
    else
        state = true;
}
bool Server::get_state(){
    return state;
}
